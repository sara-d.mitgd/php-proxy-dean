PHP HTTP Proxy is a PHP script for downloading webpages from a given server  and
making them available on some other domain. It will update the  content  of  web
pages before serving them to prevent broken links to other pages or resources.

The main use case for this is exposing the content served by some web server  in
your internal network using that other working web server with PHP support  that
happens to also be connected to the wider internet (this is not as uncommon  as
it might sound initially).

This project is a fork of
[another project](https://sourceforge.net/projects/php-proxy/),  with  the  same
name, that had it's last release in 2002.

# Installation and Configuration #

 1. Download this repository
   - Using GIT: `git clone https://gitlab.com/alexander255/php-proxy-dean.git`
   - [ZIP file](https://gitlab.com/alexander255/php-proxy-dean/repository/archive.zip?ref=master)
 2. Extract the ZIP file (if not using GIT) and open the resulting directory
 3. Copy the file `config.dist.php` to a new file named `config.php`
 4. Edit the file `config.php` using a text editor:
    1. Change the URL next to `$server` to the URL from which you plan to run *PHP Proxy* from
    2. Change the URL next to `$redirectIP` to the URL which you plan to access through the proxy
    3. Save and close the file
 5. Upload all files in the *PHP Proxy* directory to a location that is visible at the `$server` directory